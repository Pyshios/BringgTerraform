# Data source to get the AWS account ID
data "aws_caller_identity" "current" {}

# Create an S3 bucket for Thanos metrics storage
resource "aws_s3_bucket" "thanos_bucket" {
  bucket = "prodlogslbringg"
  tags = {
    Name        = "Thanos Metrics Storage"
    Environment = "Production"
  }
}

# Configure the ownership controls for the S3 bucket
resource "aws_s3_bucket_ownership_controls" "thanos_ownership_controls" {
  bucket = aws_s3_bucket.thanos_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

# Set the ACL for the S3 bucket to private
resource "aws_s3_bucket_acl" "thanos_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.thanos_ownership_controls]
  bucket     = aws_s3_bucket.thanos_bucket.id
  acl        = "private"
}

# Intelligent Tiering Configuration for the entire bucket
resource "aws_s3_bucket_intelligent_tiering_configuration" "thanos_bucket_tiering" {
  bucket = aws_s3_bucket.thanos_bucket.id
  name   = "EntireBucket"
  tiering {
    access_tier = "ARCHIVE_ACCESS"
    days        = 125  # Days to move to ARCHIVE_ACCESS after last access
  }
  tiering {
    access_tier = "DEEP_ARCHIVE_ACCESS"
    days        = 180  # Days to move to DEEP_ARCHIVE_ACCESS after last access
  }
}

# Lifecycle configuration to automatically delete old metrics
resource "aws_s3_bucket_lifecycle_configuration" "thanos_bucket_lifecycle" {
  bucket = aws_s3_bucket.thanos_bucket.id
  rule {
    id      = "expire_old_metrics"
    status  = "Enabled"
    expiration {
      days = 1825  # Automatically delete objects older than 5 years
    }
  }
}

# Server-side encryption configuration for the S3 bucket using SSE-S3
resource "aws_s3_bucket_server_side_encryption_configuration" "thanos_encryption" {
  bucket = aws_s3_bucket.thanos_bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# IAM policy for Thanos access to the S3 bucket
resource "aws_iam_policy" "thanos_s3_access" {
  name        = "ThanosS3AccessPolicy"
  path        = "/"
  description = "Policy granting access to S3 for Thanos components"
  policy      = data.aws_iam_policy_document.thanos_s3_access_policy.json
}

# IAM policy document for Thanos access
data "aws_iam_policy_document" "thanos_s3_access_policy" {
  statement {
    actions   = ["s3:PutObject", "s3:GetObject", "s3:ListBucket", "s3:GetBucketVersioning", "s3:PutBucketVersioning", "s3:PutBucketLifecycle", "s3:GetBucketLifecycle", "s3:DeleteObject", "s3:AbortMultipartUpload"]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.thanos_bucket.arn}", "${aws_s3_bucket.thanos_bucket.arn}/*"]
  }
}

# IAM role for Thanos which assumes that it can be assumed by AWS services like EC2
resource "aws_iam_role" "thanos_s3_role" {
  name = "ThanosS3Role"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = {"Service": ["ec2.amazonaws.com"]}
        Action    = "sts:AssumeRole"
      }
    ]
  })
}

# Attach the access policy to the Thanos IAM role
resource "aws_iam_role_policy_attachment" "thanos_s3_access_attach" {
  role       = aws_iam_role.thanos_s3_role.name
  policy_arn = aws_iam_policy.thanos_s3_access.arn
}

# Create an IAM instance profile for the Thanos IAM role
resource "aws_iam_instance_profile" "thanos_instance_profile" {
  name = "ThanosInstanceProfile"
  role = aws_iam_role.thanos_s3_role.name
}
