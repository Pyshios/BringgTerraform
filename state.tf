module "terraform_state_backend" {
  source = "cloudposse/tfstate-backend/aws"
  # Cloud Posse recommends pinning every module to a specific version
  version = "1.4.1"
  namespace  = "TEST"
  stage      = "homework"
  name       = "terraform"
  attributes = ["state"]

  terraform_backend_config_file_path = "."
  terraform_backend_config_file_name = "backend.tf"
  force_destroy                      = false
}

provider "aws" {
  alias  = "region-main"
  region = "eu-central-1"  # Adjust this to the region you originally used
}