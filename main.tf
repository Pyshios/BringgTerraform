locals {
  project_name = var.project_name
  amazon_host  = "amazon-linux"
  ubuntu_host  = "analytics-host"
}

################## Create Security Group MASTER NODE ##################



data "http" "myip" {
  url = "https://ipv4.icanhazip.com"
}

resource "aws_security_group" "master_ec2_sg" {
  provider    = aws.region-main
  name        = var.base_security_group_name
  description = "MasterNode"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow 443 for Gitlab CIDR WebAPI"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["34.74.90.64/28" , "34.74.226.0/24"]
  }

  ingress {
    description = "Allow 80 for Gitlab CIDR WebAPI"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["34.74.90.64/28" , "34.74.226.0/24"]
  }

}

################## Create Security Group MASTER NODE ##################


resource "aws_security_group" "slave_ec2_sg" {
  provider    = aws.region-main
  name        = "slave-sg"
  description = "MasterNode"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "Allow 22 from Master EC2 IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [for ip in module.master.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
################## Create Security Group For Agent NODE ##################

resource "aws_security_group" "internal_ec2_sg_agent" {
  provider    = aws.region-main
  name        = "AgentSG"
  description = "Allow agents to communicate within the network"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "SSH access from Master EC2 IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [for ip in module.master.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }

  ingress {
    description = "HTTPS access to agent"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [for ip in module.slave-monitoring.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Node exporter access on port 9100"
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = [for ip in module.slave-monitoring.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Application access on port 8080"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [for ip in module.slave-monitoring.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Logging service access on port 3100"
    from_port   = 3100
    to_port     = 3100
    protocol    = "tcp"
    cidr_blocks = [for ip in module.slave-monitoring.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Web traffic on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [for ip in module.slave-monitoring.ubuntu_host_public_ip : "${ip}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
################## Create Security Group For Monitoring NODE ##################

resource "aws_security_group" "internal_ec2_sg_monitoring" {
  provider    = aws.region-main
  name        = "MonitoringSG"
  description = "Allow monitoring to communicate within the network"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "SSH access from Master EC2 IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [for ip in module.master.ubuntu_host_public_ip : "${ip}/32"]
  }

  ingress {
    description = "Allow 22 from our public IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }

  ingress {
    description = "HTTPS access to agent"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }

  ingress {
    description = "Logging service access on port 3100"
    from_port   = 3100
    to_port     = 3100
    protocol    = "tcp"
    cidr_blocks = [for ip in data.aws_instances.agent_ips.public_ips : "${ip}/32"]
  }

  ingress {
    description = "Web traffic on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.response_body)}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
################## Gather IP from type agent ##################

data "aws_instances" "agent_ips" {
  instance_tags = {
    Type = "agent"
  }

  instance_state_names = ["running", "stopped"]
}
################## Deploy master ##################

module "master" {
  source = "./modules/master_node"

  instance_type          = var.instance_type
  key_name               = "Master"
  type_ami               = data.aws_ami.ubuntu_ami.id
  vpc_security_group_ids = [aws_security_group.master_ec2_sg.id]
  subnet_id = aws_subnet.public_subnet.id



  tags = {
    "Name" = "${local.ubuntu_host}-Master"
    "OS"   = local.ubuntu_host
  }
}

################## Deploy slave agent large size ##################


module "slave-agent" {
  source = "./modules/slave_node_agent"

  instance_type          = "t3a.xlarge"
  key_name               = "Slave"
  type_ami               = data.aws_ami.ubuntu_ami.id
  vpc_security_group_ids = [aws_security_group.internal_ec2_sg_agent.id ]
  subnet_id = aws_subnet.public_subnet.id



  tags = {
    "Name" = "${local.ubuntu_host}-large-env"
    "OS"   = local.ubuntu_host
    "Environment" = "production"
    "Type" = "agent"
    "Size" = "large"
  }

  depends_on = [module.master]
}


################## Deploy slave agent medium size##################

module "slave-agent-medium" {
  source = "./modules/slave_node_agent"

  instance_type          = "t2.medium"
  key_name               = "slavemedium"
  type_ami               = "ami-09e63a8bcf64c1a94"
  vpc_security_group_ids = [aws_security_group.internal_ec2_sg_agent.id ]
  subnet_id = aws_subnet.public_subnet.id



  tags = {
    "Name" = "${local.ubuntu_host}-medium-env"
    "OS"   = local.ubuntu_host
    "Environment" = "production"
    "Type" = "agent"
    "Size" = "medium"
  }

  depends_on = [module.master]
}

################## Deploy slave agent monitoring node ##################

module "slave-monitoring" {
  source = "./modules/slave_node_monitoring"

  instance_type          = "t2.medium"
  key_name               = "Slave-monitoring"
  type_ami               = data.aws_ami.ubuntu_ami.id
  vpc_security_group_ids = [aws_security_group.internal_ec2_sg_monitoring.id]
  subnet_id = aws_subnet.public_subnet.id



  tags = {
    "Name" = "${local.ubuntu_host}-monitoring-server"
    "OS"   = local.ubuntu_host
    "Environment" = "production"
    "Type" = "monitoring"
  }

  depends_on = [module.master]
}







