variable "project_name" {
  description = "Name of the project"
  type        = string
  default     = "Ansible-Dynamic-Inventory"
}

variable "instance_type" {
  description = "Name of the project"
  type        = string
  default     = "t2.medium"
}

variable "key_name" {
  description = "Name of the key pair"
  type        = string
  default     = "aws-ec2-access"
}

variable "amazon_linux_host_count" {
  description = "Number of amazon linux host"
  type        = number
  default     = 2
}

variable "ubuntu_host_count" {
  description = "Number of ubuntu host"
  type        = number
  default     = 3
}

variable "private_key_location" {
  description = "Location of the private key"
  type        = string
  default     = "/home/ec2-user/.ssh/aws-ec2-access.pem"
}

variable "sg_ports" {

  description = "Define the ports and protocols for the security group"
  type        = list(any)
  default = [
    {
      "port" : 22,
      "protocol" : "tcp"
    },
    {
      "port" : -1,
      "protocol" : "icmp"
    },
    {
      "port" : 443,
      "protocol" : "tcp"
    },
    {
      "port" : 80,
      "protocol" : "tcp"
    }
  ]

}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "172.21.0.0/19"
}

variable "subnet_cidr" {
  description = "CIDR block for the subnet"
  type        = string
  default     = "172.21.0.0/23"
}

variable "public_ip_on_launch" {
  description = "Should public IP be mapped on launch for instances in the subnet"
  type        = bool
  default     = true
}

variable "tags" {
  description = "Common tags for all resources"
  type        = map(string)
  default     = {
    Owner = "Analytics"
    Environment = "Production"
  }
}

variable "vpc_id" {
  description = "The VPC ID where the security groups will be created"
  type        = string
}

variable "base_security_group_name" {
  description = "Name of the base security group for EC2 instances"
  type        = string
  default     = "default-sg-ssh-ec2"
}

variable "ssh_cidr_blocks" {
  description = "CIDR blocks to allow SSH access"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "icmp_cidr_blocks" {
  description = "CIDR blocks to allow ICMP access"
  type        = list(string)
  default     = ["0.0.0.0/0"]
}
