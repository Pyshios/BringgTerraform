resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


data "tls_public_key" "private_key_pem" {
  private_key_pem = tls_private_key.ssh.private_key_pem
}

resource "local_file" "private_key" {
  content         = data.tls_public_key.private_key_pem.private_key_pem
  filename        = "${var.key_name}-private.pem"
  file_permission = "0400"
}

resource "aws_key_pair" "deployer" {
  key_name   = var.key_name
  public_key = tls_private_key.ssh.public_key_openssh
}

#resource "local_file" "public_key" {
#  content  = tls_private_key.ssh.public_key_openssh
#  filename = "${var.key_name}-public.pem"
#}


resource "aws_instance" "ec2" {

  ami                    = var.type_ami
  instance_type          = var.instance_type
  key_name               = aws_key_pair.deployer.key_name
  vpc_security_group_ids = var.vpc_security_group_ids
  tags                   = var.tags
  iam_instance_profile   = aws_iam_instance_profile.ansible_ec2_profile.name
  subnet_id              = var.subnet_id


  connection {
    type        = "ssh"
    user        = "ubuntu"
    host        = self.public_ip
    private_key = file("${var.key_name}-private.pem") # Path to your private SSH key
    agent       = false
    timeout     = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      # Set non-interactive frontend for apt-get to prevent it from asking for user input
      "export DEBIAN_FRONTEND=noninteractive",

      # Install needrestart to manage service restarts
      "sudo apt install needrestart -y",

      # Configure needrestart to automatically restart services without user interaction
      "sudo sed -i 's/#$nrconf{restart} = '\"'\"'i'\"'\"';/$nrconf{restart} = '\"'\"'a'\"'\"';/g' /etc/needrestart/needrestart.conf",

      # Run needrestart to automatically restart any services if needed
      "sudo needrestart -r a",

      #      # Update system repositories
      #      "sudo apt update -y",

      # Install software-properties-common for managing repositories
      "sudo apt install software-properties-common -y",

      # Add Ansible's Personal Package Archive (PPA) and update package lists
      "sudo add-apt-repository --yes --update ppa:ansible/ansible",
      "sudo apt update",

      # Install Ansible
      "sudo apt install ansible -y",

      # Install Python pip
      "sudo apt install python3-pip -y",
      "sudo pip3 install boto3 botocore",

      # Create the Ansible inventory directory if it doesn't exist
      "sudo mkdir -p /etc/ansible",

      "sudo bash -c 'echo \"plugin: amazon.aws.aws_ec2\" > /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"cache: false\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"use_ssm_inventory: true\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"regions:\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"  - eu-central-1\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"iam_role_arn: ${aws_iam_role.ansible_ec2_role.arn}\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"keyed_groups:\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"  - prefix: tag\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"    key: tags\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"  - prefix: instance_type\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"    key: instance_type\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"hostnames:\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"- tag:private_name\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"compose:\" >> /etc/ansible/aws_ec2.yaml'",
      "sudo bash -c 'echo \"  ansible_host: public_ip_address\" >> /etc/ansible/aws_ec2.yaml'"
    ]
  }


}

resource "null_resource" "generate_static_inventory" {
  provisioner "local-exec" {
    command = templatefile("${path.module}/static-inventory-template.tpl", {
      ubuntu = aws_instance.ec2[*].public_ip
    })
  }

  depends_on = [
    aws_instance.ec2,
  ]
}
resource "aws_iam_policy" "ansible_ec2_policy" {
  name        = "AnsibleEC2InventoryPolicy"
  description = "Policy to allow EC2 instance to describe EC2 for Dynamic Inventory"

  policy = jsonencode({
    Version   = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = [
          "ec2:DescribeInstances",
          "ec2:DescribeTags",
          "ssm:GetInventory"
        ],
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role" "ec2_assume_role" {
  name = "EC2AssumeRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role" "ansible_ec2_role" {
  name = "AnsibleEC2Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = {
          AWS = aws_iam_role.ec2_assume_role.arn
        },
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_policy" "ec2_assume_policy" {
  name        = "EC2AssumePolicy"
  description = "Policy that allows assumption of AnsibleEC2Role"

  policy = jsonencode({
    Version   = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = "sts:AssumeRole",
        Resource  = aws_iam_role.ansible_ec2_role.arn
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ec2_assume_policy_attachment" {
  role       = aws_iam_role.ec2_assume_role.name
  policy_arn = aws_iam_policy.ec2_assume_policy.arn
}

resource "aws_iam_role_policy_attachment" "ansible_ec2_policy_attachment" {
  role       = aws_iam_role.ansible_ec2_role.name
  policy_arn = aws_iam_policy.ansible_ec2_policy.arn
}

resource "aws_iam_instance_profile" "ansible_ec2_profile" {
  name = "AnsibleEC2Profile"
  role = aws_iam_role.ec2_assume_role.name
}
