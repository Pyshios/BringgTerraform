output "ssh_key" {
  value = tls_private_key.ssh.public_key_openssh
}

output "ubuntu_host_public_ip" {
  description = "public ip's of host"
  value       = aws_instance.ec2[*].public_ip
}

output "ubuntu_host_private_ip" {
  description = "private ip's of host"
  value       = aws_instance.ec2[*].private_ip
}

output "ansible_ec2_role_arn" {
  description = "The ARN of the IAM Role for Ansible EC2"
  value       = aws_iam_role.ansible_ec2_role.arn
}