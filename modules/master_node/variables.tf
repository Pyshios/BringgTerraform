variable "type_ami" {
  description = "Name of OS Image used by EC2"
}
variable "instance_type" {
  description = "Instance type , will define its hardware configuration"
}

variable "vpc_security_group_ids" {
  description = "Securitu Groups used by on this VPC that will be attached on EC2"
}

variable "key_name" {
  description = "Key name that will be create to be attached on the EC2"

}

variable "tags" {
  description = "A map of tags to add to all resources"
}

variable "subnet_id" {
  description = "Subnet ID for the EC2"
}
