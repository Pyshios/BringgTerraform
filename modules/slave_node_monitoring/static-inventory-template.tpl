cat << EOF >> ../../static_inventory


[ubuntu]
%{ for ip in ubuntu ~}
${ip}
%{ endfor ~}

[linux:children]
ubuntu

EOF