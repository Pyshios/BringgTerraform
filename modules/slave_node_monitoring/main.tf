resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


data "tls_public_key" "private_key_pem" {
  private_key_pem = tls_private_key.ssh.private_key_pem
}

resource "local_file" "private_key" {
  content         = data.tls_public_key.private_key_pem.private_key_pem
  filename        = "${var.key_name}-private.pem"
  file_permission = "0400"
}

resource "aws_key_pair" "deployer" {
  key_name   = var.key_name
  public_key = tls_private_key.ssh.public_key_openssh
}

resource "aws_instance" "ec2" {

  ami                    = var.type_ami
  instance_type          = var.instance_type
  key_name               = aws_key_pair.deployer.key_name
  vpc_security_group_ids = var.vpc_security_group_ids
  tags                   = var.tags
  subnet_id              = var.subnet_id


  provisioner "file" {
    source      = "${path.module}/../../publicmaster.pub"
    destination = "/tmp/publicmaster.pub"  # Temp location on the remote server

    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file("${var.key_name}-private.pem") # Path to your private SSH key
      agent       = false
      timeout     = "2m"
    }
  }

#  # Attach an encrypted EBS volume
#  ebs_block_device {
#    device_name           = "/dev/sda1"  # Adjust the device name as per your AMI and instance type
#    volume_size           = 100  # Set the volume size to 100 GB
#    volume_type           = "gp2"  # General purpose SSD
#    delete_on_termination = true  # Ensure the volume gets deleted on instance termination
#    encrypted             = true  # Enable encryption
#  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    host        = self.public_ip
    private_key = file("${var.key_name}-private.pem") # Path to your private SSH key
    agent       = false
    timeout     = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      # Set non-interactive frontend for apt-get to prevent it from asking for user input
      "export DEBIAN_FRONTEND=noninteractive",

      # Install needrestart to manage service restarts
      "sudo apt install needrestart -y",

      # Configure needrestart to automatically restart services without user interaction
      "sudo sed -i 's/#$nrconf{restart} = '\"'\"'i'\"'\"';/$nrconf{restart} = '\"'\"'a'\"'\"';/g' /etc/needrestart/needrestart.conf",

      # Run needrestart to automatically restart any services if needed
      "sudo needrestart -r a",

      # Install software-properties-common for managing repositories
      "sudo apt install software-properties-common -y",

      # Add Ansible's Personal Package Archive (PPA) and update package lists
      "sudo add-apt-repository --yes --update ppa:ansible/ansible",
      "sudo apt update -y",

      # Install Ansible
      "sudo apt install ansible -y",

      # Install Python pip
      "sudo apt install python3-pip -y",
      "sudo pip3 install boto3 botocore",

      # Create the .ssh directory if it doesn't exist and append the public key
      "sudo mkdir -p /home/ubuntu/.ssh",
      "sudo cat /tmp/publicmaster.pub >> /home/ubuntu/.ssh/authorized_keys",
      "sudo chmod 600 /home/ubuntu/.ssh/authorized_keys",
      "sudo chmod 700 /home/ubuntu/.ssh",
      "sudo rm /tmp/publicmaster.pub",  # Optional: Clean up after adding the key

      # Install dependencies for Docker
      "sudo apt install -y apt-transport-https ca-certificates curl software-properties-common",

      # Add Docker GPG key
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",

      # Add Docker repository
      "sudo add-apt-repository --yes \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",

      # Update package list again
      "sudo apt update",

      # Install Docker
      "sudo apt install -y docker-ce",

      # Start and enable Docker service
      "sudo systemctl start docker",
      "sudo systemctl enable docker",

      # Add your user to the docker group (optional, to run Docker without sudo)
      "sudo usermod -aG docker $USER",

      # Install Docker Compose (optional)
      "sudo curl -L \"https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",

      # Test Docker installation
      "docker --version",

      # Test Docker Compose installation
      "docker-compose --version"
    ]
  }



}

resource "null_resource" "generate_static_inventory" {
  provisioner "local-exec" {
    command = templatefile("${path.module}/static-inventory-template.tpl", {
      ubuntu = aws_instance.ec2[*].public_ip
    })
  }

  depends_on = [
    aws_instance.ec2,
  ]
}
