output "ssh_key" {
  value = tls_private_key.ssh.public_key_openssh
}

output "host_public_ip" {
  description = "public ip's of host"
  value       = aws_instance.ec2[*].public_ip
}

output "ubuntu_host_private_ip" {
  description = "private ip's of host"
  value       = aws_instance.ec2[*].private_ip
}