project_name            = "Ansible-Dynamic-Inventory"
instance_type           = "t2.micro"
key_name                = "aws-ec2-access"
private_key_location    = ".ssh/"
sg_ports = [
  {
    "port" : 22,
    "protocol" : "tcp"
  },
  {
    "port" : -1,
    "protocol" : "icmp"
  },
  {
    "port" : 443,
    "protocol" : "tcp"
  },
  {
    "port" : 80,
    "protocol" : "tcp"
  }
]


vpc_cidr               = "172.21.0.0/19"
subnet_cidr            = "172.21.0.0/23"
public_ip_on_launch    = true
tags                   = {
  Name        = "analytics-vpc"
  Environment = "Production"
}


#Master node
vpc_id = "vpc-12345678"
base_security_group_name = "MasterNode SG"
