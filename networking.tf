# VPC
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  tags       = var.tags
}

# Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id
  cidr_block              = var.subnet_cidr
  map_public_ip_on_launch = var.public_ip_on_launch
  tags                    = var.tags
}

# Internet Gateway
resource "aws_internet_gateway" "main_ig" {
  vpc_id = aws_vpc.main_vpc.id
  tags   = var.tags
}

# Route Table for Public Subnet (Internet Access)
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_ig.id
  }

  tags = var.tags
}

# Association of Route Table to VPC
resource "aws_main_route_table_association" "main_route_table_association" {
  vpc_id         = aws_vpc.main_vpc.id
  route_table_id = aws_route_table.public_route_table.id
}
